package com.axles.mvp.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.axles.mvp.AxlesApplication;
import com.axles.mvp.mvp.presenter.BasePresenter;
import com.axles.mvp.mvp.view.BaseView;

public abstract class BaseActivity<VIEW extends BaseView,
        PRESENTER extends BasePresenter>
        extends AppCompatActivity {

    private static final int LOADER_ID = 101;
    private boolean isPaused;
    protected PRESENTER presenter;
    protected VIEW view;
    protected AxlesApplication app;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (AxlesApplication) getApplication();
        view = initView();
        presenter = initPresenter();
        presenter.attachView(view);
    }

    @Override
    protected void onResume() {
        super.onResume();
        isPaused = false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isPaused = true;
        view.hideProgress();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
        presenter = null;
    }

    public VIEW getView() {
        return view;
    }

    protected abstract VIEW initView();

    protected abstract PRESENTER initPresenter();

    protected static Intent getBaseStartIntent(Context context, Class<? extends BaseActivity> activityClass, boolean clearStack) {
        Intent intent = new Intent(context, activityClass);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (clearStack) {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }
        return intent;
    }

    public void openUrlInWeb(Uri uri) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(uri);
            startActivity(intent);
        } catch (Exception ex) {
            view.showMessage(ex.getLocalizedMessage());
        }
    }
}
