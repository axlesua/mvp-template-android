package com.axles.mvp.ui.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.axles.mvp.mvp.presenter.BasePresenter;
import com.axles.mvp.mvp.view.BaseView;

public abstract class BaseFragment<VIEW extends BaseView,
        PRESENTER extends BasePresenter>
        extends Fragment {

    private static final int LOADER_ID = 102;
    protected PRESENTER presenter;
    protected VIEW view;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        view = initView();
    }

    @Override
    public void onPause() {
        super.onPause();
        view.hideProgress();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
        presenter = null;
    }

    public VIEW getFragmentView() {
        return view;
    }

    protected abstract VIEW initView();

    protected abstract PRESENTER initPresenter();

    protected void openUriInWeb(Uri uri) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(uri);
            startActivity(intent);
        } catch (Exception ex) {
            view.showMessage(ex.getLocalizedMessage());
        }
    }
}
