package com.axles.mvp.ui.activities;

import android.os.Bundle;

import com.axles.mvp.AxlesApplication;
import com.axles.mvp.R;
import com.axles.mvp.databinding.ActivityTestBinding;
import com.axles.mvp.mvp.presenter.BasePresenter;
import com.axles.mvp.mvp.presenter.TestPresenter;
import com.axles.mvp.mvp.view.BaseView;
import com.axles.mvp.mvp.view.TestViewImpl;
import android.databinding.DataBindingUtil;

import javax.inject.Inject;

public class TestActivity extends BaseActivity {

    @Inject
    TestPresenter testPresenter;

    private ActivityTestBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AxlesApplication.getInstance().getApplicationComponent().inject(this);
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_test);
        binding.buttonTest.setOnClickListener(v -> {
            testPresenter.getData("lviv");
        });
    }

    @Override
    protected BaseView initView() {
        return new TestViewImpl (this) {

        };
    }

    @Override
    protected BasePresenter initPresenter() {
        return testPresenter;
    }
}
