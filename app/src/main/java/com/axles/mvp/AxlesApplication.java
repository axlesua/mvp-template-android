package com.axles.mvp;

import android.app.Application;

import com.axles.mvp.di.ApplicationComponent;
import com.axles.mvp.di.ApplicationModule;
import com.axles.mvp.di.DaggerApplicationComponent;

public class AxlesApplication extends Application {
    private static AxlesApplication instance;

    public static AxlesApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public ApplicationComponent getApplicationComponent() {
        return DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(BuildConfig.API_URL))
                .build();
    }

}
