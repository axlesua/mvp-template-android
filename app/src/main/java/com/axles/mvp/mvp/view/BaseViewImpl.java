package com.axles.mvp.mvp.view;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Toast;

import com.axles.mvp.common.ProgressDialogHelper;

import static android.widget.Toast.LENGTH_SHORT;

public abstract class BaseViewImpl implements BaseView {

    private Activity activity;
    private Fragment fragment;
    private View view;
    private ProgressDialogHelper progressDialogHelper;

    public BaseViewImpl(Activity activity) {
        this.activity = activity;
        init();
    }

    public BaseViewImpl(Fragment fragment) {
        this.fragment = fragment;
        init();
    }

    public BaseViewImpl(View view) {
        this.view = view;
        init();
    }

    @Override
    public void showMessage(String message) {
        if (getSnackBarBackground() == null) {
            return;
        }
        Toast.makeText(getContext(), message, LENGTH_SHORT).show();
    }

    @Override
    public void showMessage(int messageResId) {
        if (getContext() == null) {
            return;
        }
        showMessage(getContext().getString(messageResId));
    }

    @Override
    public void showProgress() {
        if (getContext() == null) {
            return;
        }
        progressDialogHelper.showProgress(getContext(), "Loading");
    }

    @Override
    public void showProgress(String message) {
        progressDialogHelper.showProgress(getContext(), message);
    }

    @Override
    public void showProgress(int messageResId) {
        if (getContext() == null) {
            return;
        }
        showProgress(getContext().getString(messageResId));
    }

    @Override
    public void showProgress(String message, String title) {
        progressDialogHelper.showProgress(getContext(), message, title);
    }

    @Override
    public void showProgress(int messageResId, int titleResId) {
        if (getContext() == null) {
            return;
        }
        showProgress(getContext().getString(messageResId), getContext().getString(titleResId));
    }

    @Override
    public void hideProgress() {
        progressDialogHelper.hideProgress();
    }

    private Context getContext() {
        if (activity != null) {
            return activity;
        } else if (fragment != null) {
            return fragment.getContext();
        } else if (view != null) {
            return view.getContext();
        }
        return null;
    }

    private void init() {
        progressDialogHelper = new ProgressDialogHelper();
    }

    private View getSnackBarBackground() {
        if (activity != null) {
            return activity.findViewById(android.R.id.content);
        } else if (view != null) {
            return view;
        } else if (fragment != null) {
            return fragment.getView();
        }
        return null;
    }

}
