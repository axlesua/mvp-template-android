package com.axles.mvp.mvp.presenter;

import android.os.Looper;
import android.support.annotation.NonNull;

import com.axles.mvp.common.BackgroundThread;
import com.axles.mvp.mvp.view.BaseView;

import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.subscriptions.CompositeSubscription;

public abstract class BasePresenter<VIEW extends BaseView> {

    protected VIEW view;
    protected Scheduler scheduler;

    public void attachView(@NonNull VIEW view) {
        this.view = view;
        onViewAttached();
    }

    public void detachView() {
        onViewDetached();
        this.view = null;
    }

    public void destroy() {
        //TODO
    }

    abstract void onViewAttached();
    abstract void onViewDetached();

    protected Scheduler getSchedulerInstance() {
        if (scheduler == null) {
            BackgroundThread backgroundThread = new BackgroundThread();
            backgroundThread.start();
            Looper backgroundLooper = backgroundThread.getLooper();
            scheduler = AndroidSchedulers.from(backgroundLooper);
        }
        return scheduler;
    }
}
