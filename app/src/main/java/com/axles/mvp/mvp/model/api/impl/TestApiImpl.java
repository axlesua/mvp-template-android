package com.axles.mvp.mvp.model.api.impl;

import com.axles.mvp.mvp.model.api.TestApi;

import javax.inject.Inject;

import retrofit2.Retrofit;
import rx.Observable;

public class TestApiImpl implements TestApi {
    private Retrofit retrofit;

    @Inject
    public TestApiImpl(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    @Override
    public Observable getWeather(String q, String appId) {
        return retrofit.create(TestApi.class).getWeather(q, appId);
    }
}
