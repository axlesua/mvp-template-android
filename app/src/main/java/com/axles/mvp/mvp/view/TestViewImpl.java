package com.axles.mvp.mvp.view;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.View;

public abstract class TestViewImpl extends BaseViewImpl implements TestView {
    public TestViewImpl(Activity activity) {
        super(activity);
    }

    public TestViewImpl(Fragment fragment) {
        super(fragment);
    }

    public TestViewImpl(View view) {
        super(view);
    }
}
