package com.axles.mvp.mvp.presenter;

import com.axles.mvp.mvp.model.DataManager;
import com.axles.mvp.mvp.model.api.TestApi;
import com.axles.mvp.mvp.model.dto.TestResponse;
import com.axles.mvp.mvp.view.TestView;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class TestPresenter extends BasePresenter<TestView> {

    private TestApi api;
    private DataManager dataManager;

    public TestPresenter(TestApi api, DataManager dataManager) {
        this.api = api;
        this.dataManager = dataManager;
    }

    @Override
    void onViewAttached() {
        view.showMessage("Done");
    }

    @Override
    void onViewDetached() {

    }

    public void getData(String city) {
        api.getWeather(city, dataManager.getAppId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<TestResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.showMessage(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(TestResponse o) {
                        if (view != null) {
                            view.showMessage("Hooray");
                        }
                    }
                });
    }
}
