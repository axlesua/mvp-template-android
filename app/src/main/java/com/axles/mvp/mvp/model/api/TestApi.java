package com.axles.mvp.mvp.model.api;

import com.axles.mvp.mvp.model.dto.TestResponse;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface TestApi {
    @GET("data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22")
    Observable<TestResponse> getWeather(@Query("q") String q, @Query("appid") String appId);
}
