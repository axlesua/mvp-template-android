package com.axles.mvp.di;

import android.support.annotation.NonNull;

import com.axles.mvp.mvp.model.DataManager;
import com.axles.mvp.mvp.model.DataManagerImpl;
import com.axles.mvp.mvp.model.api.TestApi;
import com.axles.mvp.mvp.model.api.impl.TestApiImpl;
import com.axles.mvp.mvp.presenter.TestPresenter;

import java.io.IOException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

@Module
public class ApplicationModule {
    private String apiUrl;

    public ApplicationModule(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    @Provides
    @Singleton
    public TestPresenter provideTestPresenter(TestApi testApi, DataManager dataManager) {
        return new TestPresenter(testApi, dataManager);
    }

    @Provides
    @Singleton
    public DataManager provideDataManager() {
        return new DataManagerImpl();
    }

    @Provides
    @Singleton
    public TestApi provideTestApi(Retrofit retrofit) {
        return new TestApiImpl(retrofit);
    }

    @Provides
    @Singleton
    public Retrofit providesRetrofit(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(apiUrl)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(JacksonConverterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    public OkHttpClient provideOkHttpClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request request = chain.request();
                        okhttp3.Response response = chain.proceed(request);
                        if (response.code() == 401) {
                            //TODO
                            return response;
                        }
                        return response;
                    }
                })
                .addInterceptor(getHttpLoggingInterceptor())
                .build();
    }

    private HttpLoggingInterceptor getHttpLoggingInterceptor() {
        HttpLoggingInterceptor logInterceptor = new HttpLoggingInterceptor();
        logInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logInterceptor;
    }

}
