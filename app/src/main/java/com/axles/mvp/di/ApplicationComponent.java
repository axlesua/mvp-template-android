package com.axles.mvp.di;

import com.axles.mvp.ui.activities.BaseActivity;
import com.axles.mvp.ui.activities.TestActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        ApplicationModule.class
})
public interface ApplicationComponent {
    void inject(TestActivity activity);
}
